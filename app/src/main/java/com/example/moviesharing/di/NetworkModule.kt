package com.example.moviesharing.di

import com.example.moviesharing.data.remote.YoutubeService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class NetworkModule() {

    @Singleton
    @Provides
    fun provideYoutubeService(): YoutubeService {
        return YoutubeService().create()
    }
}