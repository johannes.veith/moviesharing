package com.example.moviesharing.utilities

class Constants {
    companion object {
        const val ACCOUNT_NAME = "account-name"
        const val DB_NAME = "moviesharing-db"
        const val PREFERENCES = "preferences"
    }
}