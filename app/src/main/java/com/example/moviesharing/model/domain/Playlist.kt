package com.example.moviesharing.model.domain

data class Playlist(
    val title: String,
    val movies: List<Movie>
)
