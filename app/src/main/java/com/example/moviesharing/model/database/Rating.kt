package com.example.moviesharing.model.database

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Rating(
    @PrimaryKey val movieId: String,
    var rating: Int
)
