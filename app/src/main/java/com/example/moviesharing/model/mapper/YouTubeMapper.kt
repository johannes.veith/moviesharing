package com.example.moviesharing.model.mapper

import com.example.moviesharing.model.domain.Movie
import com.google.api.services.youtube.model.Video

object YouTubeMapper {

    fun mapVideo(video: Video): Movie {
        return Movie(
            id = video.id,
            title = video.snippet.title,
            imageUrl = video.snippet.thumbnails.high.url,
            description = video.snippet.description
        )
    }
}