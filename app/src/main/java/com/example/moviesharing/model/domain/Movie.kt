package com.example.moviesharing.model.domain

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Movie(
    val id: String,
    val title: String,
    val imageUrl: String,
    val description: String
) : Parcelable
