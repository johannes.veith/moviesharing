package com.example.moviesharing.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.moviesharing.model.database.Favorite
import com.example.moviesharing.model.database.Rating

/**
 * The Room database for this app
 */
@Database(
    entities = [
        Favorite::class,
        Rating::class],
    version = 2,
    exportSchema = false
)

abstract class AppDatabase : RoomDatabase() {
    abstract fun movieDao(): MovieDao
}