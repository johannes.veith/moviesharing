package com.example.moviesharing.data.local

import androidx.room.*
import com.example.moviesharing.model.database.Favorite
import com.example.moviesharing.model.database.Rating
import kotlinx.coroutines.flow.Flow

@Dao
interface MovieDao {

    @Query("SELECT * FROM favorite")
    fun getFavorites(): Flow<List<Favorite>>

    @Query("SELECT * FROM favorite WHERE movieId = :movieId LIMIT 1")
    fun getFavoriteById(movieId: String): Favorite?

    @Delete
    fun delete(fav: Favorite)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(fav: Favorite)

    @Query("SELECT * FROM rating WHERE movieId = :movieId LIMIT 1")
    fun getRatingById(movieId: String): Rating?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(rating: Rating)
}