package com.example.moviesharing.data.remote

import com.example.moviesharing.model.domain.Playlist
import com.example.moviesharing.model.mapper.YouTubeMapper
import com.google.api.client.extensions.android.http.AndroidHttp
import com.google.api.client.http.HttpTransport
import com.google.api.client.json.JsonFactory
import com.google.api.client.json.jackson2.JacksonFactory
import com.google.api.services.youtube.YouTube
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Singleton

@Singleton
class YoutubeService {
    private lateinit var service: YouTube
    private val refreshIntervalMs = 1000000L

    val playlists: Flow<List<Playlist>> = flow {
        while (true) {
            emit(getPlaylists())
            delay(refreshIntervalMs)
        }
    }

    private fun getPlaylists(): List<Playlist> {
        val playlists = mutableListOf<Playlist>()

        val playlistResult = service.playlists()
            .list("snippet")
            .setKey("AIzaSyBuzIVoCQXFg_kVrg9AGoJhjqFz8kQ3dTA")
            .setChannelId("UCoUIPSnOUyxF6aGz4_wUenw")
            .execute()

        playlistResult.items.forEach { playlist ->
            val builder = StringBuilder()

            val playlistItemResult = service.playlistItems()
                .list("snippet,contentDetails")
                .setKey("AIzaSyBuzIVoCQXFg_kVrg9AGoJhjqFz8kQ3dTA")
                .setPlaylistId(playlist.id)
                .execute()

            playlistItemResult.items.forEach {
                builder.append(it.contentDetails.videoId).append(",")
            }

            val videosResult = service.videos()
                .list("snippet")
                .setKey("AIzaSyBuzIVoCQXFg_kVrg9AGoJhjqFz8kQ3dTA")
                .setId(builder.toString())
                .execute()

            val movies = videosResult.items.map { YouTubeMapper.mapVideo(it) }

            playlists.add(Playlist(playlist.snippet.title, movies))
        }

        return playlists
    }

    fun create(): YoutubeService {
        val transport: HttpTransport = AndroidHttp.newCompatibleTransport()
        val jsonFactory: JsonFactory = JacksonFactory.getDefaultInstance()

        service = YouTube.Builder(transport, jsonFactory, null)
            .setApplicationName("MovieSharing")
            .build()

        return this
    }
}