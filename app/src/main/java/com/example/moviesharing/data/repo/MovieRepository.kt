package com.example.moviesharing.data.repo

import com.example.moviesharing.data.local.MovieDao
import com.example.moviesharing.data.remote.YoutubeService
import com.example.moviesharing.model.database.Favorite
import com.example.moviesharing.model.database.Rating
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MovieRepository @Inject constructor(
    youTubeService: YoutubeService, private val movieDao: MovieDao
) {

    val playlists = youTubeService.playlists.flowOn(Dispatchers.Default)

    val favorites: Flow<List<Favorite>> = movieDao.getFavorites()

    fun updateFavorite(isFavorite: Boolean, movieId: String) {
        if (isFavorite) {
            movieDao.insert(Favorite(movieId))
        } else {
            movieDao.delete(Favorite(movieId))
        }
    }

    fun getFavoriteByMovie(movieId: String): Favorite? {
        return movieDao.getFavoriteById(movieId)
    }

    fun updateRatingById(rating: Int, movieId: String) {
        movieDao.insert(Rating(movieId, rating))
    }

    fun getRatingByMovie(movieId: String): Rating? {
        return movieDao.getRatingById(movieId)
    }
}