package com.example.moviesharing.ui.movies

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.moviesharing.data.repo.MovieRepository
import com.example.moviesharing.model.domain.Movie
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MoviesViewModel @Inject constructor(repo: MovieRepository) : ViewModel() {

    val playlists = repo.playlists

    private val _navigateToMovieDetail = MutableLiveData<Movie?>()
    val navigateToMovieDetail
        get() = _navigateToMovieDetail

    fun onMovieClicked(movie: Movie) {
        _navigateToMovieDetail.value = movie
    }

    fun onMovieNavigated() {
        _navigateToMovieDetail.value = null
    }
}