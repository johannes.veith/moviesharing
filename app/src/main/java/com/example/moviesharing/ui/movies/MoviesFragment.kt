package com.example.moviesharing.ui.movies

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.moviesharing.databinding.FragmentMoviesBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@AndroidEntryPoint
class MoviesFragment : Fragment() {

    private val viewModel: MoviesViewModel by viewModels()
    private var _binding: FragmentMoviesBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentMoviesBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupAdapter()

        handleNavigation()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setupAdapter() {
        val indeterminateBar = binding.indeterminateBar
        val errorText = binding.errorText

        val playlistAdapter = MoviesPlaylistAdapter(MovieClickListener { movie ->
            viewModel.onMovieClicked(movie)
        }
        )

        binding.playlistList.adapter = playlistAdapter

        lifecycleScope.launch {
            viewModel.playlists
                .catch {
                    Log.e("YoutubeService", it.message, it)
                    errorText.visibility = View.VISIBLE
                    indeterminateBar.visibility = View.GONE
                }
                .collect {
                    playlistAdapter.submitList(it)
                    errorText.visibility = View.GONE
                    indeterminateBar.visibility = View.GONE
                }
        }
    }

    private fun handleNavigation() {
        viewModel.navigateToMovieDetail.observe(viewLifecycleOwner, { movie ->
            movie?.let {

                findNavController().navigate(
                    MoviesFragmentDirections.actionNavigationMoviesToNavigationDetails(it, it.title)
                )

                viewModel.onMovieNavigated()
            }
        })
    }
}