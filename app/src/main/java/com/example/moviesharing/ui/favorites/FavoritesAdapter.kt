package com.example.moviesharing.ui.favorites

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.moviesharing.databinding.FavoriteItemBinding
import com.example.moviesharing.model.domain.Movie
import com.example.moviesharing.ui.movies.MovieClickListener
import java.util.*

class FavoritesAdapter(
    private val listener: MovieClickListener,
    private val viewModel: FavoritesViewModel
) :
    ListAdapter<Movie, FavoritesAdapter.FavoriteViewHolder>(PlaylistDiffCallback()), Filterable {

    private lateinit var allFavorites: List<Movie>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoriteViewHolder {
        return FavoriteViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: FavoriteViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item, listener)
    }

    fun submitFilterableList(list: MutableList<Movie>) {
        allFavorites = list
        super.submitList(list)
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun publishResults(charSequence: CharSequence?, filterResults: FilterResults) {
                (filterResults.values as? List<Movie>)?.let {
                    submitList(it)
                    viewModel.setIsSearchResultsEmpty(
                        it.isEmpty() && ::allFavorites.isInitialized && charSequence != ""
                    )
                }
            }

            override fun performFiltering(charSequence: CharSequence?): FilterResults {
                val queryString = charSequence?.toString()?.lowercase(Locale.getDefault())

                val filterResults = FilterResults()

                if (::allFavorites.isInitialized) {
                    filterFavorites(queryString, filterResults)
                }

                return filterResults
            }

            private fun filterFavorites(queryString: String?, filterResults: FilterResults) {
                if (queryString == null || queryString.isEmpty()) {
                    filterResults.values = allFavorites
                } else {
                    filterResults.values = allFavorites.filter {
                        it.title.contains(queryString, true)
                    }
                }
            }
        }
    }

    class FavoriteViewHolder private constructor(private val binding: FavoriteItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(
            item: Movie,
            clickListener: MovieClickListener
        ) {
            binding.movie = item
            binding.clickListener = clickListener
            binding.movieImage.load(item.imageUrl)
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): FavoriteViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = FavoriteItemBinding.inflate(layoutInflater, parent, false)
                return FavoriteViewHolder(binding)
            }
        }
    }
}

class PlaylistDiffCallback : DiffUtil.ItemCallback<Movie>() {
    override fun areItemsTheSame(oldItem: Movie, newItem: Movie): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Movie, newItem: Movie): Boolean {
        return oldItem == newItem
    }
}