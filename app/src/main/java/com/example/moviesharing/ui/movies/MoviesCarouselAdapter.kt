package com.example.moviesharing.ui.movies

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.moviesharing.databinding.MovieCarouselItemBinding
import com.example.moviesharing.model.domain.Movie

class MoviesCarouselAdapter(
    private val movies: List<Movie>,
    private val clickListener: MovieClickListener
) :
    RecyclerView.Adapter<MoviesCarouselAdapter.MovieViewHolder>() {

    override fun getItemCount(): Int {
        return movies.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        return MovieViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        val item = movies[position]
        holder.bind(item, clickListener)
    }

    class MovieViewHolder private constructor(private val binding: MovieCarouselItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(
            item: Movie,
            clickListener: MovieClickListener
        ) {
            binding.movie = item
            binding.movieImage.load(item.imageUrl)
            binding.clickListener = clickListener
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): MovieViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = MovieCarouselItemBinding.inflate(layoutInflater, parent, false)
                return MovieViewHolder(binding)
            }
        }
    }
}

class MovieClickListener(val clickListener: (movie: Movie) -> Unit) {
    fun onClick(movie: Movie) = clickListener(movie)
}