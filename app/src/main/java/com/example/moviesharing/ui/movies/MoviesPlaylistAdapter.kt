package com.example.moviesharing.ui.movies

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.moviesharing.databinding.PlaylistListItemBinding
import com.example.moviesharing.model.domain.Playlist

class MoviesPlaylistAdapter(private val clickListener: MovieClickListener) :
    ListAdapter<Playlist, MoviesPlaylistAdapter.PlaylistViewHolder>(PlaylistDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlaylistViewHolder {
        return PlaylistViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: PlaylistViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item, clickListener)
    }

    class PlaylistViewHolder private constructor(private val binding: PlaylistListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(
            item: Playlist,
            clickListener: MovieClickListener
        ) {
            binding.playlist = item
            binding.moviesHorizontalList.adapter = MoviesCarouselAdapter(item.movies, clickListener)
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): PlaylistViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = PlaylistListItemBinding.inflate(layoutInflater, parent, false)
                return PlaylistViewHolder(binding)
            }
        }
    }
}

class PlaylistDiffCallback : DiffUtil.ItemCallback<Playlist>() {
    override fun areItemsTheSame(oldItem: Playlist, newItem: Playlist): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Playlist, newItem: Playlist): Boolean {
        return oldItem == newItem
    }
}