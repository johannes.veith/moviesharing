package com.example.moviesharing.ui.favorites

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.moviesharing.databinding.FragmentFavoritesBinding
import com.example.moviesharing.ui.movies.MovieClickListener
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@AndroidEntryPoint
class FavoritesFragment : Fragment(), SearchView.OnQueryTextListener {

    private val viewModel: FavoritesViewModel by viewModels()
    private var _binding: FragmentFavoritesBinding? = null
    private var searchQuery = ""

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentFavoritesBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupAdapter()
        setupSearchView()
        handleNavigation()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onResume() {
        (binding.favoritesList.adapter as FavoritesAdapter).filter.filter(searchQuery)
        super.onResume()
    }

    private fun setupAdapter() {
        val favoritesAdapter = FavoritesAdapter(MovieClickListener { movie ->
            viewModel.onMovieClicked(movie)
        }, viewModel)

        val indeterminateBar = binding.indeterminateBarFavorites

        binding.favoritesList.adapter = favoritesAdapter

        lifecycleScope.launch {
            viewModel.favMovies.collect {
                favoritesAdapter.submitFilterableList(it.toMutableList())
                indeterminateBar.visibility = View.GONE
            }
        }
    }

    private fun setupSearchView() {
        binding.favsSearchView.setOnQueryTextListener(this)

        viewModel.resultsEmpty.observe(viewLifecycleOwner, {
            if (it) {
                binding.noResultsText.visibility = View.VISIBLE
            } else {
                binding.noResultsText.visibility = View.GONE
            }
        })
    }

    private fun handleNavigation() {
        viewModel.navigateToMovieDetail.observe(viewLifecycleOwner, { movie ->
            movie?.let {

                findNavController().navigate(
                    FavoritesFragmentDirections.actionNavigationFavoritesToNavigationDetails(
                        it,
                        it.title
                    )
                )

                viewModel.onMovieNavigated()
            }
        })
    }

    private fun filterContactsList(query: String?) {
        (binding.favoritesList.adapter as FavoritesAdapter).filter.filter(query)
        query?.let { searchQuery = query }
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        filterContactsList(query)
        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        filterContactsList(newText)
        return true
    }
}