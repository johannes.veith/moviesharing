package com.example.moviesharing.ui.moviedetails

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources
import androidx.lifecycle.lifecycleScope
import androidx.navigation.navArgs
import coil.load
import com.example.moviesharing.R
import com.example.moviesharing.databinding.ActivityMovieDetailsBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@AndroidEntryPoint
class MovieDetailsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMovieDetailsBinding
    private val viewModel: MovieDetailsViewModel by viewModels()

    private val args: MovieDetailsActivityArgs by navArgs()

    private var favState = false
    private var movieId = ""

    private var optionsMenu: Menu? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMovieDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        handleNavArgs()
        setupClickListeners()
        observeStates()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.movie_details_options_menu, menu)

        optionsMenu = menu
        setMenuItemIcon(favState)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.toggle_favorite_option -> {
                favState = !favState
                setMenuItemIcon(favState)
                viewModel.updateFavorite(favState, movieId)
                return true
            }
            android.R.id.home -> {
                this.onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun handleNavArgs() {
        args.movie.let {
            movieId = it.id

            viewModel.getFavorite(it.id)
            viewModel.getRating(it.id)

            supportActionBar?.title = it.title

            binding.bgImage.load(it.imageUrl)
            binding.thumbnail.load(it.imageUrl)

            binding.movieTitle.text = it.title
            binding.textMovie.text = it.description
        }
    }

    private fun observeStates() {
        lifecycleScope.launch {
            viewModel.rating.collect {
                updateStarIcons(it)
            }
        }

        lifecycleScope.launch {
            viewModel.favState.collect {
                setMenuItemIcon(it)
            }
        }
    }

    private fun setupClickListeners() {
        binding.ratingStar1.setOnClickListener {
            viewModel.updateRating(1, movieId)
            updateStarIcons(1)
        }

        binding.ratingStar2.setOnClickListener {
            viewModel.updateRating(2, movieId)
            updateStarIcons(2)
        }

        binding.ratingStar3.setOnClickListener {
            viewModel.updateRating(3, movieId)
            updateStarIcons(3)
        }

        binding.ratingStar4.setOnClickListener {
            viewModel.updateRating(4, movieId)
            updateStarIcons(4)
        }

        binding.ratingStar5.setOnClickListener {
            viewModel.updateRating(5, movieId)
            updateStarIcons(5)
        }
    }

    private fun setMenuItemIcon(isFav: Boolean) {
        optionsMenu?.let {
            val favItem = it.findItem(R.id.toggle_favorite_option)

            if (isFav) {
                favItem.icon =
                    AppCompatResources.getDrawable(this, R.drawable.ic_favorites)
            } else {
                favItem.icon =
                    AppCompatResources.getDrawable(this, R.drawable.ic_favorites_border)
            }
        }
    }

    private fun updateStarIcons(rating: Int?) {
        when (rating) {
            1 -> {
                binding.ratingStar1.setImageResource(R.drawable.ic_star)
                binding.ratingStar2.setImageResource(R.drawable.ic_star_border)
                binding.ratingStar3.setImageResource(R.drawable.ic_star_border)
                binding.ratingStar4.setImageResource(R.drawable.ic_star_border)
                binding.ratingStar5.setImageResource(R.drawable.ic_star_border)
            }
            2 -> {
                binding.ratingStar1.setImageResource(R.drawable.ic_star)
                binding.ratingStar2.setImageResource(R.drawable.ic_star)
                binding.ratingStar3.setImageResource(R.drawable.ic_star_border)
                binding.ratingStar4.setImageResource(R.drawable.ic_star_border)
                binding.ratingStar5.setImageResource(R.drawable.ic_star_border)
            }
            3 -> {
                binding.ratingStar1.setImageResource(R.drawable.ic_star)
                binding.ratingStar2.setImageResource(R.drawable.ic_star)
                binding.ratingStar3.setImageResource(R.drawable.ic_star)
                binding.ratingStar4.setImageResource(R.drawable.ic_star_border)
                binding.ratingStar5.setImageResource(R.drawable.ic_star_border)
            }
            4 -> {
                binding.ratingStar1.setImageResource(R.drawable.ic_star)
                binding.ratingStar2.setImageResource(R.drawable.ic_star)
                binding.ratingStar3.setImageResource(R.drawable.ic_star)
                binding.ratingStar4.setImageResource(R.drawable.ic_star)
                binding.ratingStar5.setImageResource(R.drawable.ic_star_border)
            }
            5 -> {
                binding.ratingStar1.setImageResource(R.drawable.ic_star)
                binding.ratingStar2.setImageResource(R.drawable.ic_star)
                binding.ratingStar3.setImageResource(R.drawable.ic_star)
                binding.ratingStar4.setImageResource(R.drawable.ic_star)
                binding.ratingStar5.setImageResource(R.drawable.ic_star)
            }
            else -> {
                binding.ratingStar1.setImageResource(R.drawable.ic_star_border)
                binding.ratingStar2.setImageResource(R.drawable.ic_star_border)
                binding.ratingStar3.setImageResource(R.drawable.ic_star_border)
                binding.ratingStar4.setImageResource(R.drawable.ic_star_border)
                binding.ratingStar5.setImageResource(R.drawable.ic_star_border)
            }
        }
    }
}