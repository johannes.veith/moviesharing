package com.example.moviesharing.ui.moviedetails

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.moviesharing.data.repo.MovieRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MovieDetailsViewModel @Inject constructor(
    private val repo: MovieRepository
) : ViewModel() {

    private val _rating = MutableStateFlow<Int?>(null)
    val rating
        get() = _rating

    private val _favState = MutableStateFlow(false)
    val favState
        get() = _favState

    fun getFavorite(movieId: String) {
        viewModelScope.launch(Dispatchers.IO) {
            _favState.value = repo.getFavoriteByMovie(movieId) != null
        }
    }

    fun getRating(movieId: String) {
        viewModelScope.launch(Dispatchers.IO) {
            _rating.value = repo.getRatingByMovie(movieId)?.rating
        }
    }

    fun updateFavorite(favState: Boolean, movieId: String) {
        viewModelScope.launch(Dispatchers.IO) {
            repo.updateFavorite(favState, movieId)
        }
    }

    fun updateRating(rating: Int, movieId: String) {
        viewModelScope.launch(Dispatchers.IO) {
            repo.updateRatingById(rating, movieId)
        }
    }
}