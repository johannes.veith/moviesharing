package com.example.moviesharing.ui.favorites

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.moviesharing.data.repo.MovieRepository
import com.example.moviesharing.model.domain.Movie
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FavoritesViewModel @Inject constructor(
    private val repo: MovieRepository
) : ViewModel() {

    init {
        viewModelScope.launch(Dispatchers.IO) {
            repo.playlists
                .catch { _isError.value = true }
                .collect { playlists ->
                    repo.favorites.collect { favorites ->
                        _isError.value = false
                        _favMovies.value = playlists
                            .map { it.movies }
                            .flatten()
                            .filter { movie -> favorites.any { movie.id == it.movieId } }
                    }
                }
        }
    }

    private val _favMovies = MutableStateFlow(emptyList<Movie>())
    val favMovies: StateFlow<List<Movie>> = _favMovies

    private val _isError = MutableStateFlow(false)
    val isError: StateFlow<Boolean> = _isError

    private val _navigateToMovieDetail = MutableLiveData<Movie?>()
    val navigateToMovieDetail
        get() = _navigateToMovieDetail

    private val _resultsEmpty = MutableLiveData<Boolean>(false)
    val resultsEmpty
        get() = _resultsEmpty

    fun setIsSearchResultsEmpty(isEmpty: Boolean) {
        _resultsEmpty.value = isEmpty
    }

    fun onMovieClicked(movie: Movie) {
        _navigateToMovieDetail.value = movie
    }

    fun onMovieNavigated() {
        _navigateToMovieDetail.value = null
    }
}